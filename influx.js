require('dotenv').config();

const influx=require('influx');

//influx configuration
const influxDB=new influx.InfluxDB({
    database:process.env.DATABASE,
    host:process.env.DBHOST,
    port:process.env.DBPORT,
});

module.exports=influxDB;