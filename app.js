const findConsumption=require('./ConsuptionRate').findConsumption;
const express=require('express');
const port=process.env.HPORT || 8080;
const app=express();
const path=require('path');
const moment=require('moment');
const addConsuptionToExcelsheet=require('./excel').createDetailsExcel;
app.use(express.urlencoded({extended:true}));
app.use(express.json())


app.post('/api',async function(req,res){     
    const {measurement,sensor,start,end}=req.body;
    //If start date is greater than end date
    if(moment(end).valueOf()<moment(start).valueOf()){
        return res.status(400).json({data:"start date is greater than end date",message:"Error in Consuption caluation"});
    }
    //formula
    var ans=[];
    var c=0;
    for(var i=0;i<measurement.length;i++){
        for(var j=0;j<sensor.length;j++){
            var val =await findConsumption(start,end,measurement[i],sensor[j]);
            var obj={
                measurement:measurement[i],
                sensor:sensor[j],
                start:start,
                end:end,
                consumptionRate:val
            }
            ans[c]=obj;
            c++;
       }
    }
    //insertdata(JSON.stringify(ans),"apiOutput");
    addConsuptionToExcelsheet(JSON.stringify(ans),JSON.stringify(req.body));
    const fileName = 'Details.xlsx'; 
    const filePath = path.join(__dirname, fileName);

    
    res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    res.setHeader('Content-Disposition', `attachment; filename="${fileName}"`);

    res.sendFile(filePath, (err) => {
      if (err) {
        console.error('Error sending file:', err);
      } else {
        console.log('File sent successfully');
      }
    })
    //return res.status(200).json({data:ans,message:`Consuption caluated done between range of ${req.body.start} to ${req.body.end}`});
    
    //return res.redirect('back');
});

app.get('/file',function(req,res){
    const fileName = 'Details.xlsx'; 
    const filePath = path.join(__dirname, fileName);

    res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    res.setHeader('Content-Disposition', `attachment; filename="${fileName}"`);

    res.sendFile(filePath, (err) => {
      if (err) {
        console.error('Error sending file:', err);
      } else {
        console.log('File sent successfully');
      }
    })
})
app.get('/',function(req,res){
    return res.send("<h1>Docker container running</h1>");
})

app.listen(port,function(err){
    if(err){
        console.log("Error in the port");
        return;
    }
    console.log("Sever is running at port : ",port);
})