#Install node
FROM node:20

#Working directory
WORKDIR /app

#Copy package.json
COPY package*.json ./

#install packages
RUN npm install

COPY . .

ENV TZ=Asia/Kolkata
ENV DBHOST=127.0.0.1

EXPOSE 8000

CMD ["node","app.js"]