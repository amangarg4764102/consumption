require('dotenv').config();

const mqtt = require('mqtt');
const insert=require('./store_in_database').insertdata;
// MQTT broker information
const broker = process.env.MQTTSERVER;
const options = {
  clientId: 'Subscriber', // Unique client ID
};

// Connect to the MQTT broker
const client = mqtt.connect(broker, options);
const topic=process.env.TOPIC;

client.on('connect',function(){
  client.subscribe(topic);
  console.log('client is subscribe the topic');
})

// Callback function when a message is received
client.on('message', (topic, message) => {
  //console.log("Message Received : ",JSON.parse(message));
  insert(message);
  
});