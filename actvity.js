require('dotenv').config();
const mqtt = require('mqtt');
const { getLastDataPacketTime,setLastDataPacketTime,setConnectionTimeout} =require('./redis');

//time out for 10 second
const timeOut=8;
const client = mqtt.connect(process.env.MQTTSERVER, {
  clientId: 'device_activity',
});

client.on('connect', () => {
    console.log("connected to broker ")
    client.subscribe(process.env.TOPIC);
})

client.on('message', (topic, message) => {
      const Obj = JSON.parse(message);
     console.log(Obj.time,"message recived activity")
     setLastDataPacketTime(Obj.time);
});

setInterval(async () => {
      try {
        const lastDataPacketTime = await getLastDataPacketTime();
        //console.log(lastDataPacketTime)
        var nowtime=(Date.now().valueOf())
        const timeDiff=(nowtime.valueOf()-lastDataPacketTime)/1000;
         console.log(timeDiff)
        if(timeDiff>timeOut){
          await setConnectionTimeout(Date.now());
          const payloadObj={
              device:"INEM_DEMO",
              time:Date.now(),
              data:[{
                tag:'RSSI',
                value:-1
              }]
          }
          const payload=JSON.stringify(payloadObj);
          client.publish(process.env.TOPIC, payload);
         console.log("device active");
          
      }
    } catch (error) {
      console.error('Error:', error);
    }
},10000);