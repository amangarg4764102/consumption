require('dotenv').config();
const influxDB=require('./influx')
const ExcelJS=require('exceljs');
const moment=require('moment');
//  --------------------------------------------------      CRUD operation-----------

//influx fetch data ==> read data
fetchdata=async function(database){
    try{
        
        const data=await influxDB.query(`SELECT * FROM ${database}`);
        //code of excel workbook=file
        const Workbook=new ExcelJS.Workbook();
        //worksheet
        const WorkSheet=Workbook.addWorksheet('device_details');
        WorkSheet.columns=[
            {header:'Time',key:'time',width:'30',style:{numFmt:'yyyy-mm-dd hh:mm:ss'}},
            {header:'Sensor',key:'sensor',width:'10'},
            {header:'Value',key:'value',width:'10'}
        ]
        //add data to excel
        data.map(val=>{
            WorkSheet.addRow(val);
        });
        WorkSheet.getRow(1).eachCell(cell=>{
            cell.font={bold:true}
        })
        //add workbook
        await Workbook.xlsx.writeFile('details.xlsx');
    }catch(err){
        console.log(err);
    }
}

//influx for create data
module.exports.insertdata=async function(obj){
    
    try{
        obj=JSON.parse(obj);
      
            for (let index = 0; index < obj.data.length; index++) {
                
                const element = obj.data[index];
                
                await influxDB.writePoints([{
                measurement:obj.device,
                time:obj.time,
                tags:{sensor:element.tag},
                fields:{value:element.value}
                
            },]);
        }
        console.log("Message save to database");
    }catch(err){
        console.log(err)
    }
}
fetchdata("INEM_DEMO");